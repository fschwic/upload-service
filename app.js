/* 
 * upload service
 * 
 * Written in 2015 by Frank Schwichtenberg fschwic@googlemail.com
 * 
 */
var http = require('http');
//var https = require('https');
var url = require('url');
var express = require('express');
var handlebars  = require('express-handlebars');
var multer  = require('multer');
var fs = require('fs');
var crypto = require('crypto');
var elasticsearch = require('elasticsearch');
var bodyParser = require('body-parser');
var qrcode = require('qrcode');
var contentDisposition = require('content-disposition');

var port = process.env.PORT || 8081;
var host = process.env.HOST || "0.0.0.0";
var accessHostPort = "tmp.EPOKO.net"; // set empty to use host:port from http-request
var baseDirectory = ".";
var uploadPath = "/uploads/";
var uploadBaseDirectory = baseDirectory + uploadPath;
var fileMaxSizeMB = 400;
var maxAllowedDownloads = 9; // if exceeded by client, allowedDownloads set to 1

var el = new elasticsearch.Client({
  host: 'elasticsearch:9200',
  log: 'warning'
});

/*
 * Used internally by multer (mulitpart/form-data handler) and
 * below for plain posts of a file.
 */
function fileSaveName(fieldname, filename) {
  var hash = crypto.createHash('sha256');
  hash.update("" + Math.random());
  var tmp = hash.digest('hex');
  var path = "";
  while (tmp.length > 0){
    path += tmp.substr(0, 4) + "/";
    tmp = tmp.substr(4);
    // console.log(path + "---" + tmp);
    // mkdir uploadBaseDirectory + path
    fs.mkdirSync(uploadBaseDirectory + path);
  }
  var fileSaveName;
  if (filename){
    fileSaveName = path + filename;
  }
  else{
    fileSaveName = path + Date.now();
  }
  return fileSaveName;
}

/*
 * Writes metadata for new file to elasticsearch.
 */
function registerFile(file, location, allowedDownloads){
  var count = 1;
  if(allowedDownloads){
    count = allowedDownloads;
  }
  el.create({
    index: 'uploads',
    type: 'file',
    id: file,
    body: {
      path: file,
      location: location,
      count: count,
      published_at: Date.now()
    }
  }, function (error, response) {
    console.log(error);
  });
}

/*
 * Decrements the download counter for a file in elasticsearch.
 * Removes the file if count becomes 0.
 */
function decrementFile(file){
  el.get({
    index: 'uploads',
    type: 'file',
    id: file
  }, function (error, response) {
    if(error){
      console.log("Can not read file info: " + error);
      fs.unlink(uploadBaseDirectory + file, function(){console.log("removed: " + file)});
    }
    else{
      console.log("response._source.count: " + response._source.count);
      var curCount = response._source.count;
      if(curCount < 2){
        fs.unlink(uploadBaseDirectory + file, function(){console.log("removed: " + file)});
      }
      el.update({
        index: 'uploads',
        type: 'file',
        id: file,
        body: {
          // put the partial document under the `doc` key
          doc: {
            count: (curCount -1)
          }
        }
      }, function (error, response) {
        if(error){
          console.log("Could not decrement count after reading file info: " + error);
        }
      });
    }
  });
}

function sendCorsHeaders(req, res, next){
  var origin = '*';
  if(req.headers.origin){
    origin = req.headers.origin;
  }
  res.header("Access-Control-Allow-Origin", origin);
  res.header("Access-Control-Allow-Methods","GET,POST");
  res.header("Access-Control-Allow-Credentials", "true");

  next();
};


/* the APP */
var app = express();
app.use(express.static('static'));
app.use(bodyParser.raw({ limit: fileMaxSizeMB + 'mb' })); // bodyparser don't restricts multipart

/* templating */
var hbs = handlebars.create({
  defaultLayout: 'main'
});
app.engine('handlebars', hbs.engine);
app.set('view engine', 'handlebars');

/* mulitpart-formdata handler */
app.use(multer({
  dest: uploadBaseDirectory,
  limits: {
    fieldNameSize: 100,
    files: 1,
    fileSize: (fileMaxSizeMB * 1024 * 1024) // multer restricts only multipart
  },
  onFileSizeLimit: function (file) {
    console.log('Filesize exceeded: ' + file.originalname + '. Removing ./' + file.path);
    fs.unlink('./' + file.path); // delete the partially written file
    file.fileSizeExceeded = true;
  },
  rename: fileSaveName
}));

/*
 * here comes the app
 */

/*
 * CORS
 */
app.all('*', sendCorsHeaders);

/*
 * Delivers uploaded files.
 */
app.get(uploadPath + '*', function (req, res, next){
  var file = decodeURI(req.path.substr(uploadPath.length));
  var filename = file.split(/[\\/]/).pop();
  var extension = filename.split('.').pop();
  //console.log("request to " + uploadPath + file + " Filename is " + filename + " and Extension is " + extension);
  console.log(req.query);
  console.log(req.header('Accept'));

  if( ! fs.existsSync(uploadBaseDirectory + file) ){
    console.log("File does not exist.");
    res.status(404);
    res.render('not-found', {
      filename: filename
    });
  }
  else if( req.query.download || !( req.header('Accept') && req.accepts('html') ) ) {
    res.sendFile(file, { 
      root: uploadBaseDirectory,
      headers: {
        'Content-disposition': 'attachment; filename=' + filename
      }
    }, function (err) {
      if (err) {
        console.log("Error Status: " + err.status);
        console.log(err);
        res.status(err.status);
        if(err.status == 404){
          res.render('not-found');
        }
        else{
          res.end();
        }
        next();
      }
      else {
        console.log('Sent:', file);
        decrementFile(file);
      }
    });
  }
  else{
    // deliver page showing download link
    var locationHost = accessHostPort.length > 0 ? accessHostPort : req.headers.host;
    var location = req.protocol + "://" + locationHost + req.url + "?download=1";
    el.get({
      index: 'uploads',
      type: 'file',
      id: file
    }, function (error, response) {
      if(error){
        console.log(error);
      }
      // TODO handle error?
      var curCount = 1;
      if(response && response._source){
        curCount = response._source.count;
      }

      if(curCount === 0){
        res.status(404);
        res.render('not-found', {
          filename: filename
        });
      }
      else{
        qrcode.toDataURL(location, {errorCorrectLevel: 'minimum'}, function(error,dataURL){
          if(error){
            return console.log('Error: ',error);
          }
          res.render('download', {
            location: location,
            filename: filename,
            count: curCount,
            qrcode: dataURL
          });
        });
      }

    });
  }
});

/*
 * Check uploaded files.
 */
app.head(uploadPath + '*', function (req, res, next){
  res.status(405);
  res.end();
});

/*
 * Root. Redirect to startpage at /upload
 */
app.get('/', function(req, res){
  res.redirect('/upload');
});

/*
 * Delivers start/upload page.
 */
app.get('/upload', function (req, res) {
  res.render('upload');
})

/*
 * Delivers FAQ page.
 */
app.get('/faq', function (req, res) {
  res.render('faq', {
    fileMaxSizeMB: fileMaxSizeMB,
    maxAllowedDownloads: maxAllowedDownloads
  });
})

/*
 * Endpoint for sending file from HTML form. 
 * Multer prepares request properties.
 */
app.post('/file-upload', function (req, res){
  // console.log(req.body);
  //console.log(req.files);
  if (req.files['file'].fileSizeExceeded){
    // file was not stored
    res.status(413);
    res.render('file-to-large', {
      fileMaxSizeMB: fileMaxSizeMB
    });
  }
  else {
    var file = req.files['file'].path.substr(uploadPath.length -1);
    var locationHost = accessHostPort.length > 0 ? accessHostPort : req.headers.host;
    var location = req.protocol + "://" + locationHost + "/" + req.files['file'].path;
    var allowedDownloads = req.body.count;
    if(allowedDownloads > maxAllowedDownloads){
      console.log("Fixing download count from " + allowedDownloads + " to 1.");
      allowedDownloads = 1;
    }
    registerFile(file, location, allowedDownloads);
    res.location(location);
    qrcode.toDataURL(location, {errorCorrectLevel: 'minimum'}, function(error,dataURL){
      if(error){
        return console.log('Error: ',error);
      }
      res.render('upload-success', {
        location: location,
        count: allowedDownloads,
        qrcode: dataURL
      });
    });
  }
});

/*
 * Endpoint for POSTing files in request body.
 */
app.post('/uploads', function (req, res, next){
  // not catched by multer, i.e. not multipart/form-data
  //console.log(req.headers);
  var mimeType = req.headers['content-type'];

  var file;
  if(req.headers['content-disposition']){
    var disposition = contentDisposition.parse(req.headers['content-disposition']);
    file = fileSaveName(null, disposition.parameters.filename);
  }
  else{
    var extension = "bin";
    if (mimeType){
      extension = mimeType.substr(mimeType.lastIndexOf('/')+1);
    }
    //console.log("extension: " + extension);
    file = fileSaveName() + "." + extension;
  }
  //console.log("file: " + file);
  var locationHost = accessHostPort.length > 0 ? accessHostPort : req.headers.host;
  var location = req.protocol + "://" + locationHost + "/uploads/" + file;
  var allowedDownloads = req.headers['x-allowed-downloads'];
  if(allowedDownloads > maxAllowedDownloads){
    console.log("Fixing download count from " + allowedDownloads + " to 1.");
    allowedDownloads = 1;
  }
  //console.log("allowedDownloads: " + allowedDownloads);
  // var fileSize = req.headers['content-length'];
  //console.log("length=" + fileSize);
  var writeable = fs.createWriteStream("./uploads/" + file);
  writeable.on('finish', function(){
    //console.log("finish writeable");
    registerFile(file, location, allowedDownloads);
    res.set("Location", location);
    res.status(204).end();
  });
  writeable.end(req.body);
});

/*
 * to actually serv
 */
var server = app.listen(port, host, function () {

  var host = server.address().address
  var port = server.address().port

  console.log('upload-service app listening at http://%s:%s', host, port)

})
