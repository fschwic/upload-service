### Run ###
Start a local instance of elasticsearch and

    npm install
    node app.js

The service will run without elasticsearch but will delete every file after the first access.

### Use ###

Send a file via HTML form to /file-upload or HTTP POSTS to /uploads. The latter one considers HTTP headers Content-Type and X-Allowed-Downloads (value Is a number lower than 10).